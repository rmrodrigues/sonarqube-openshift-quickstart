sonarQube-openshift-quickstart
==============================

Easily deploy SonarQube on OpenShift.

> **Note:** The Quick Start is under construction.

rhc create-app cakestrap diy-0.1 mysql-5.1 --from-code https://github.com/rmrodrigues/sonarQube-openshift-quickstart.git


[![githalytics.com alpha](https://cruel-carlota.pagodabox.com/e6eb5da6f9be977b75b82a41e1f2faf6 "githalytics.com")](http://githalytics.com/rmrodrigues/sonarQube-openshift-quickstart)
